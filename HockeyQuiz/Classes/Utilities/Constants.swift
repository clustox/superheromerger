//
//  Constants.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation

/**
 *  Struct for declaring this project's global values
 */
struct Constants {
    // Banner id for Admob
    static let admobBannerID = "ca-app-pub-8793358717797604/1458001089"
    
    // Interstitial Id for Admob
    static let admobInterstitialID = "ca-app-pub-8793358717797604/2934734287"
    
    // Admob Application ID
    static let googleApplicationID = "ca-app-pub-8793358717797604~8981267883"
    
    // Google Analytics ID
    static let googleAnalyticsID = "UA-55757458-7"
    
    // Appstore ID of this App
    static let appID = "968318705"
    
    // iTunes URL of App
    static let iTunesURL = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=\(appID)&mt=8&uo=6"
    
    // URL hosting more apps file
    static let moreAppsFileURL = ""
    
    static var categories = [String]()
}
