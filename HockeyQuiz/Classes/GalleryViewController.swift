//
//  ImageGalleryViewController.swift
//  HockeyQuiz
//
//  Created by Mac on 30/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation
import UIKit

class GalleryViewController: GAITrackedViewController {
    
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    let reuseableIdentifier = "image_cell"
    
    let itemsPerRow = 3
    
    var level = 0
    
    var images = [String]()
    
    let sectionInsets = UIEdgeInsets(top: 5.0, left: 8.0, bottom: 5.0, right: 8.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageCollectionView.dataSource = self
        self.imageCollectionView.delegate = self
        
        self.configureData()
    }
    
    func configureData() {
        
        let category = DataStorage.shared.category
        
        let path = category.imageSource.replacingOccurrences(of: " ", with: "")
        
//        let imageArray = Bundle.main.urls(forResourcesWithExtension: "png", subdirectory: path)! as [NSURL]
        
        self.images = Bundle.main.paths(forResourcesOfType: "png", inDirectory: path)
        
        self.imageCollectionView.reloadData()
        
    }
    
    @IBAction func dismissBtnTapped(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

extension GalleryViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseableIdentifier , for: indexPath) as! ImageCell
        
        let image = UIImage(named: self.images[indexPath.row])
        cell.image.image = image
        
        return cell
    }
}

extension GalleryViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.size.width
        let widthPerItem = screenWidth/3.7
        return CGSize(width: widthPerItem, height: widthPerItem * 1.5)
    }
    
}


