//
//  SelectCategoryViewController.swift
//  HockeyQuiz
//
//  Created by Mac on 14/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation

class SelectCategoryViewController: GAITrackedViewController {
    
    
    @IBOutlet weak var ivBackground: UIImageView!
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    @IBOutlet weak var ivCategoryLogo: UIImageView!
    
    @IBOutlet weak var leftArrowBtn: UIButton!
    
    @IBOutlet weak var rightArrowBtn: UIButton!
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.configureView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
    }
    
    //MARK : IBActions
    
    @IBAction func LeftArrowTapped(_ sender: UIButton) {
        
        self.count -= 1
        if DataStorage.shared.categories.count > 0 && self.count >= 0  {
            
            self.setImages()
            self.categoryNameLabel.text = DataStorage.shared.categories[count].categoryName.capitalized
            self.animateInLeft()
            
        }else {
            self.count += 1
        }
    }
    
    @IBAction func rightArrowTapped(_ sender: UIButton) {
        
        self.count += 1
        if DataStorage.shared.categories.count > 0 && self.count < DataStorage.shared.categories.count {
            
            self.setImages()
            self.categoryNameLabel.text = DataStorage.shared.categories[count].categoryName.capitalized
            self.animateInRight()
            
        }else {
            self.count -= 1
        }
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        
        let category = DataStorage.shared.categories[count]
        DataStorage.shared.category = category
        self.setLevel()
        DataStorage.shared.level = 1
        if (DatabaseHandler.loadQuiz(category: category)) {
            self.performSegue(withIdentifier: "ShowRandomGamePlaySegue", sender: category)
        }else {
            DataStorage.shared.loadQuiz()
            self.performSegue(withIdentifier: "ShowRandomGamePlaySegue", sender: category)
        }
    }
    
    
    @IBAction func levelButtonTapped(_ sender: UIButton) {
        let category = DataStorage.shared.categories[count]
        DataStorage.shared.category = category
        self.setLevel()
        if (DatabaseHandler.loadQuiz(category: category)) {
            self.performSegue(withIdentifier: "ShowLevelsSegue", sender: category)
        }else {
            DataStorage.shared.loadQuiz()
            self.performSegue(withIdentifier: "ShowLevelsSegue", sender: category)
        }
    }
    
    
    @IBAction func statsButtonTapped(_ sender: UIButton) {
        let category = DataStorage.shared.categories[count]
        DataStorage.shared.category = category
        self.performSegue(withIdentifier: "ShowStatsSegue", sender: category)
    }
    
    
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func configureView() {
        
        if DataStorage.shared.categories.count > 0 {
            
            self.categoryNameLabel.text = DataStorage.shared.categories[count].categoryName.capitalized
            self.setImages()
            self.animateInRight()
        }
    }
    
    func setLevel() {
        
        let category = DataStorage.shared.category.categoryName.replacingOccurrences(of: " ", with: "_")
        let key = category.appending("_level")
        let theLastLevel = UserDefaults.standard.integer(forKey: key)
        if theLastLevel > 0 {
            DataStorage.shared.user.currentLevel = theLastLevel
        }else {
            DataStorage.shared.user.currentLevel = 1
        }
    }
    
    func animateInRight() {
        self.categoryNameLabel.slideIn(from: kFTAnimationLeft, duration: 0.2, delegate: nil)
        self.ivCategoryLogo.slideIn(from: kFTAnimationLeft, duration: 0.2, delegate: nil)
    }
    
    func animateInLeft() {
        self.categoryNameLabel.slideIn(from: kFTAnimationRight, duration: 0.2, delegate: nil)
        self.ivCategoryLogo.slideIn(from: kFTAnimationRight, duration: 0.2, delegate: nil)
    }
    
    func setImages() {
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        
        var bgImage = DataStorage.shared.categories[count].categoryName
        bgImage = bgImage.lowercased().replacingOccurrences(of: " ", with: "_")
        
        var logoImage = DataStorage.shared.categories[count].categoryName
        logoImage = logoImage.lowercased().replacingOccurrences(of: " ", with: "_")
        
        switch (deviceIdiom) {
        case .pad:
            bgImage = bgImage.appending("_ipad")
            logoImage = logoImage.appending("_ipad")
        default:
            break
        }
        
        
        bgImage = NSString(format: "%@_bg", bgImage) as String
        let image = UIImage(named: bgImage)
        self.ivBackground.image = image
        
        logoImage = NSString(format: "%@_logo", logoImage) as String
        let logo = UIImage(named: logoImage)
        self.ivCategoryLogo.image = logo
    }

}

