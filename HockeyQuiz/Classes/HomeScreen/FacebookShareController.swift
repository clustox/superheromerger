//
//  FacebookShareController.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/30/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import FBSDKShareKit
import FBSDKLoginKit

protocol ShareControllerDelegate: class {
    func errorReceived(message: String)
}

public class ShareController: NSObject {
    // Delegate
    weak var delegate: ShareControllerDelegate?
    
    func share(onFacebook urlString: String,
               description: String) {
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logOut()
        
        guard let viewController = self.delegate as? UIViewController else {
            return
        }
        
        login.logIn(withPublishPermissions: ["publish_actions"], from: viewController, handler:{ (result: FBSDKLoginManagerLoginResult?, error: Error?) -> Void in
            if error != nil {
                print("%@", error!.localizedDescription)
            }
            else {
                let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
                content.contentURL = URL(string: urlString)
                content.contentDescription = description
                FBSDKShareDialog.show(from: viewController, with: content, delegate: self)
            }
        })
    }
}

// MARK: FBSDKSharingDelegate Conformance
extension ShareController: FBSDKSharingDelegate {
    public func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        
    }
    
    public func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
    public func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        self.delegate?.errorReceived(message: "We were not able to share it this time. Please try again.")        
    }
}
