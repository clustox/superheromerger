//
//  StatsViewController.swift
//  HockeyQuiz
//
//  Created by Mac on 23/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation

class StatsViewController: GAITrackedViewController {
    
    
    @IBOutlet weak var correctAnsLabel: UILabel!
    
    @IBOutlet weak var wrongAnsLabel: UILabel!
    
    @IBOutlet weak var skipLabel: UILabel!
    
    @IBOutlet weak var bestScoreLabel: UILabel!
    
    var category = Category()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.category = DataStorage.shared.category
        self.loadData()
    }
    
    @IBAction func dismissButtonWasClicked(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func showGalleryButtonWasClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "showGalleryViewController", sender: nil)
    }
    
    //MARK: functions
    
    func loadData() {
        
        let category = self.category.categoryName.replacingOccurrences(of: " ", with: "_")
        let bestScoreKey = category.appending("_best_score")
        let rightAnsKey = category.appending("_right_ans")
        let wrongAnsKey = category.appending("_wrong_ans")
        let skipAnswerKey = category.appending("_skip_ans")
        
        let highScore = UserDefaults.standard.integer(forKey: bestScoreKey)
        let rightAnsCount = UserDefaults.standard.integer(forKey: rightAnsKey)
        let wrongAnsCount = UserDefaults.standard.integer(forKey: wrongAnsKey)
        let skipAnsCount = UserDefaults.standard.integer(forKey: skipAnswerKey)
        
        self.correctAnsLabel.text = String(rightAnsCount)
        self.wrongAnsLabel.text = String(wrongAnsCount)
        self.skipLabel.text = String(skipAnsCount)
        self.bestScoreLabel.text = String(highScore)
    }

    
}
