//
//  CustomizedMoreAppsCellView.swift
//  GuessTheCity
//
//  Created by Saira on 12/4/15.
//  Copyright © 2015 Saira. All rights reserved.
//

import UIKit

class CustomizedMoreAppsCellView: UIView {
 
    var maAppDict: NSDictionary?
    
    init(frame: CGRect, maApp: NSDictionary) {
        self.maAppDict = maApp
        super.init(frame: frame)
        
        let appName = self.maAppDict!.object(forKey: "appName")
        let iconURL = self.maAppDict!.object(forKey: "appIconURL") as! String
        
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        imgView.image = UIImage(named: "more_apps_row_bg_iphone")
        self.addSubview(imgView)
        
        var image = UIImage(named: iconURL)
        if image == nil {
            image = UIImage(named: "default_icon")
        }
     
        let iconViewY = (frame.size.height - 80)/2
        let iconView = UIImageView(frame: CGRect(x: 10, y: iconViewY, width: 80, height: 80))
        iconView.tag = 1
        iconView.image = image
        iconView.layer.masksToBounds = true;
        iconView.layer.cornerRadius = 5.0;
        self.addSubview(iconView)
        
        var fontSize: CGFloat = 18.0
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        switch (deviceIdiom) {
        case .pad:
            fontSize = 30.0
        default:
            fontSize = 18.0
        }
        
        let originX = iconView.frame.origin.x + iconView.frame.size.width + 20
        let label = UILabel(frame: CGRect(x: originX, y: iconView.frame.origin.y, width: self.bounds.size.width - originX - 10, height: iconView.frame.size.height))
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.text = appName as? String
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        label.font = UIFont (name: "Roboto-Regular", size: fontSize)
        label.tag = 2
        
        let btn = UIButton(type: UIButtonType.custom)
        btn.addTarget(self, action: #selector(CustomizedMoreAppsCellView.goToiTunes(_:)), for: UIControlEvents.touchUpInside)
        btn.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: frame.size.height)
        self.addSubview(btn)
        self.addSubview(label)
    }

    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    func populateViewForApp(_ maApp: NSDictionary) {
        self.maAppDict = maApp
        
        let lbl = self.viewWithTag(2) as! UILabel
        let iconImgView = self.viewWithTag(1) as! UIImageView
        
        let appName = maApp.object(forKey: "appName") as! String
        lbl.text = appName
        iconImgView.image = UIImage(named: String(format: "%@", ".png", appName))
    }
    
    func goToiTunes(_ sender: AnyObject) {
        if self.connected() {
            if self.maAppDict != nil {
                let url = maAppDict?.object(forKey: "appURL") as! String
                UIApplication.shared.openURL(URL(string: url)!)
            }
        }
    }
}
