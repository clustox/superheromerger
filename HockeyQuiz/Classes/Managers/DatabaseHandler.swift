//
//  DatabaseHandler.swift
//  HockeyQuiz
//
//  Created by Mac on 16/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation
import CoreData

public class DatabaseHandler: NSObject {
    
    static func insertQuestions(questions: [Int:[Question]], category: Category) -> Bool {
    
        guard let context = UIApplication.appDelegate?.getContext() else {
            fatalError("ReInstall the App")
        }
        
        let levels = questions.keys.sorted()
        
        for level in levels {
            
            let questionsPerLevel = questions[level]
            
            for question in questionsPerLevel! {
                
                _ = DatabaseManager.insertQuestion(inManagedObjectContext: context, category: category.categoryName, question: question)
            }
        }
        return true
    }

    static func checkCategory(category: Category) -> Bool {
        
        guard let context = UIApplication.appDelegate?.getContext() else {
            fatalError("ReInstall the App")
        }
        
        return DatabaseManager.checkCategory(managedObjectContext: context, category: category.categoryName)
    }
    
    static func loadQuiz(category: Category) -> Bool {
        
        guard let context = UIApplication.appDelegate?.getContext() else {
            fatalError("ReInstall the App")
        }
        
        if (DatabaseManager.checkCategory(managedObjectContext: context, category: category.categoryName)) {
            
            DataStorage.shared.quizDict.removeAll()
            
            let levels = DatabaseManager.getLevelsCount(managedObjectContext: context, category: category.categoryName)
            
            if levels > 0 {
                
                for level in stride(from: 1, to: levels + 1, by: 1) {
                    
                    let managedQuestions = DatabaseManager.getQuestions(of: level, from: category.categoryName, managedObjectContext: context)
                    
                    DataStorage.shared.quizDict[level] = mapToQuestionModel(from: managedQuestions)
                }
            }
            
            DataStorage.shared.totalLevels = DataStorage.shared.quizDict.keys.count
            
            return true
        }
        
        return false
    }
    
    static func mapToQuestionModel(from managedQuestions: [ManagedQuestion]) -> [Question] {
        
        var questions = [Question]()
        
        for managedQuestion in managedQuestions {
            
            let question = Question.mapToQuestion(from: managedQuestion)
            questions.append(question)
        }
        
        return questions
    }
    
    static func updateQuestion(question: Question) {
        
        guard let context = UIApplication.appDelegate?.getContext() else {
            fatalError("ReInstall the App")
        }
        
        _ = DatabaseManager.updateQuestion(managedObjectContext: context, question: question)
    }
    
    static func checkProgress() -> Bool {
        
        guard let context = UIApplication.appDelegate?.getContext() else {
            fatalError("ReInstall the App")
        }
        
        let level = DataStorage.shared.level
        let category = DataStorage.shared.category
        
        let correctAns = DatabaseManager.getCorrectQuestions(managedObjectContext: context, category: category.categoryName, level: level)
        
        let totalQuestions = DatabaseManager.getQuestions(of: level, from: category.categoryName, managedObjectContext: context)
        
        if totalQuestions.count > 0 {
            
            let progress = (Double(correctAns.count) / Double(totalQuestions.count)) * 100
            
            if (progress.rounded() >= 60.0) {
                return true
            }
        }
        return false
    }
    
    static func clearData() -> Bool {
        
        guard let context = UIApplication.appDelegate?.getContext() else {
            fatalError("ReInstall the App")
        }
        
        return DatabaseManager.clearData(managedObjectContext: context)
    }
}
