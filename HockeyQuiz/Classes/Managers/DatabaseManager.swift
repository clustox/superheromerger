//
//  DatabaseManager.swift
//  HockeyQuiz
//
//  Created by Mac on 15/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation
import CoreData


// MARK: Inserting, deleting, updating Question
public class DatabaseManager: NSObject {
    
    
    
    /// Inserts Question in core data
    ///
    /// - Parameters:
    ///   - managedObjectContext: managed context
    ///   - question: question instance
    /// - Returns: instance of managed question
    static func insertQuestion(inManagedObjectContext
        managedObjectContext: NSManagedObjectContext,category: String,
                               question: Question) -> Bool {
        
        let entity =  NSEntityDescription.entity(forEntityName: "Question",
                                                 in:managedObjectContext)
        
        guard let managedQuestion = NSManagedObject(entity: entity!,
                                                    insertInto:
            managedObjectContext) as? ManagedQuestion else {
                fatalError("Could not insert a new question into the managed object context \(managedObjectContext)")
        }
        
        managedQuestion.id = Int64(question.id)
        managedQuestion.answer = question.answer
        managedQuestion.level = Int64(question.level)
        managedQuestion.optionOne = question.optionOne
        managedQuestion.optionTwo = question.optionTwo
        managedQuestion.optionThree = question.optionThree
        managedQuestion.playerName = question.playerName
        managedQuestion.category = category
        
        do {
            try managedQuestion.managedObjectContext?.save()
            
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        return true
    }
    
    
    static func checkCategory(managedObjectContext: NSManagedObjectContext, category: String) -> Bool {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "category == %@", category)
        
        do {
            let result = try managedObjectContext.fetch(fetchRequest)
            
            if result.count > 0 {
                return true
            }
            return false
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return false
    }
    
    static func getLevelsCount(managedObjectContext: NSManagedObjectContext, category: String) -> Int {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "category == %@", category)
        fetchRequest.propertiesToGroupBy = ["level"]
        fetchRequest.propertiesToFetch = ["level"]
        fetchRequest.returnsDistinctResults = true
        
        do {
            let result = try managedObjectContext.fetch(fetchRequest)
            
            return result.count
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return 0
    }
    
    static func getQuestions(of level: Int,from category: String, managedObjectContext: NSManagedObjectContext) -> [ManagedQuestion] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "level == %d AND category == %@", level,category)
        
        do {
            let result = try managedObjectContext.fetch(fetchRequest) as? [ManagedQuestion]
            
            return result!
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return []
    }
    
    static func updateQuestion(managedObjectContext: NSManagedObjectContext,
                               question: Question) -> Bool {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "id == %d", question.id)
        
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            guard let managedQuestion = results.first as? ManagedQuestion else {
                return false
            }
            print(managedQuestion.isCorrect)
            managedQuestion.isCorrect = question.isCorrect
            managedQuestion.isVisited = question.isVisited
            try managedQuestion.managedObjectContext?.save()
            return true
        } catch {
            let fetchError = error as NSError
            print(fetchError)
            return false
        }
        return false
    }
    
    static func getCorrectQuestions(managedObjectContext: NSManagedObjectContext, category: String, level: Int) -> [ManagedQuestion] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "level == %d AND category == %@ AND isCorrect == %@", level, category,NSNumber(booleanLiteral: true))
        
        do {
            let result = try managedObjectContext.fetch(fetchRequest) as! [ManagedQuestion]
            
            return result
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return []
    }

    static func getWrongQuestions(managedObjectContext: NSManagedObjectContext, category: String, level: Int) -> [ManagedQuestion] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "level == %d AND category == %@ AND isCorrect == %@ AND isVisited == %@", level, category,NSNumber(booleanLiteral: false),NSNumber(booleanLiteral: true))
        
        do {
            let result = try managedObjectContext.fetch(fetchRequest) as! [ManagedQuestion]
            
            return result
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return []
    }
    
    static func getTotalQuestions(managedObjectContext: NSManagedObjectContext, category: String, level: Int) -> [ManagedQuestion] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Question", in: managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "level == %d AND category == %@", level, category)
        
        do {
            let result = try managedObjectContext.fetch(fetchRequest) as! [ManagedQuestion]
            
            return result
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return []
    }
    
    static func clearData(managedObjectContext: NSManagedObjectContext) -> Bool {
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Question")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            try managedObjectContext.execute(request)
            try managedObjectContext.save()
            
            return true
        } catch {
            print("Error occurred while clearing database")
            return false
        }
        return false
    }
    
}
