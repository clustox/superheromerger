//
//  SelectLevelViewController.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit

class SelectLevelViewController: GAITrackedViewController {
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var ivLogo: UIImageView!
    
    @IBOutlet weak var ivBackground: UIImageView!
    
    var levels = [Int]()
    var category = Category()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.category = DataStorage.shared.category
        
        self.configureView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        levels.append(contentsOf: (DataStorage.shared.quizDict.keys).sorted())
    }

    @IBAction func dismissButtonWasPressed(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    // MARK: UIViewController Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "ShowGamePlaySegue" {
            guard let controller = segue.destination as? GamePlayViewController else {
                return
            }
            controller.forLevel = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.screenName = "Home Screen"
        self.tableView.reloadData()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureView() {
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        
        var bgImage = self.category.categoryName
        bgImage = bgImage.lowercased().replacingOccurrences(of: " ", with: "_")
        
        var logoImage = self.category.categoryName
        logoImage = logoImage.lowercased().replacingOccurrences(of: " ", with: "_")
        
        switch (deviceIdiom) {
        case .pad:
            bgImage = bgImage.appending("_ipad")
            logoImage = logoImage.appending("_ipad")
        default:
            break
        }

        bgImage = NSString(format: "%@_bg", bgImage) as String
        logoImage = NSString(format: "%@_logo", logoImage) as String
        
        
        let image = UIImage(named: bgImage)
        self.ivBackground.image = image
        
        let logo = UIImage(named: logoImage)
        self.ivLogo.image = logo

    }
}

extension SelectLevelViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return levels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "level_cell") else {
            fatalError("No cell with this identifier")
        }
        
        let level = levels[indexPath.row]
        guard let button = cell.contentView.viewWithTag(1) as? UIButton else {
            fatalError("No button added to cell")
        }
        
        button.setTitle("Level \(level)", for: .normal)
        if level <= DataStorage.shared.user.currentLevel {
            button.isEnabled = true
        } else {
            button.isEnabled = false
        }
        
        return cell
    }
}

extension SelectLevelViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let level = levels[indexPath.row]
        if level <= DataStorage.shared.user.currentLevel {
            DataStorage.shared.level = level
            self.performSegue(withIdentifier: "ShowGamePlaySegue", sender: tableView)
        }
    }
}
