//
//  ImageCell.swift
//  HockeyQuiz
//
//  Created by Mac on 30/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation

public class ImageCell: UICollectionViewCell {
    
    
    @IBOutlet weak var image: UIImageView!
}
