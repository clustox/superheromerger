//
//  Question.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import UIKit

class Question {
    
    var id = 0
    var answer = ""
    var optionOne = ""
    var optionTwo = ""
    var optionThree = ""
    var level: Int = -1
    var category = ""
    var isVisited = false
    var isCorrect = false
    var playerName = ""
    var isSkipped = false
    
    init() {}
    
    init(dictionary : NSDictionary) {
        self.optionOne = dictionary.object(forKey: "option1") as! String
        self.optionTwo = dictionary.object(forKey: "option2") as! String
        self.optionThree = dictionary.object(forKey: "option3") as! String
        self.answer = dictionary.object(forKey: "answer") as! String
        self.level = Int((dictionary.object(forKey: "level") as! String))!
        if let name = dictionary.object(forKey: "PlayerName") as? String {
            self.playerName = name
        }else {
            self.playerName = ""
        }
        self.id = Int((dictionary.object(forKey: "id") as! String))!
        
        let iPhoneImage = UIImage(named: "\(self.answer.lowercased().replacingOccurrences(of: " ", with: "_")).png")
        if iPhoneImage == nil {
            print("Missing Image Iphone: \(self.answer.lowercased().replacingOccurrences(of: " ", with: "_")).png")
        }
        
        let iPadImage = UIImage(named: "\(self.answer.lowercased().replacingOccurrences(of: " ", with: "_"))_pad.png")
        if iPadImage == nil {
            print("Missing Image Ipad: \(self.answer.lowercased().replacingOccurrences(of: " ", with: "_"))_pad.png\n")
        }
    }
    
    static func mapToQuestion(from managedQuestion: ManagedQuestion) -> Question {
        
        let question = Question()
        
        question.id = Int(managedQuestion.id)
        question.answer = managedQuestion.answer!
        question.optionOne = managedQuestion.optionOne!
        question.optionTwo = managedQuestion.optionTwo!
        question.optionThree = managedQuestion.optionThree!
        question.level = Int(managedQuestion.level)
        question.category = managedQuestion.category!
        question.isVisited = managedQuestion.isVisited
        question.isCorrect = managedQuestion.isCorrect
        question.isSkipped = managedQuestion.isSkipped
        question.playerName = managedQuestion.playerName!
        
        return question
    }
}
