//
//  DataParser.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation

protocol DataParserDelegate: class {
    /// Callback function for parsing questions from given xml
    ///
    /// - Parameters:
    ///   - events: List of questions arranged on levels if succesfully created else empty List
    func questionsReturned(questions: [Int: [Question]])
}

class DataParser: NSObject, XMLParserDelegate {
    
    // Delegate
    weak var delegate: DataParserDelegate?
    
    var quizDict = [Int: [Question]]()
    var questionDict: NSMutableDictionary = NSMutableDictionary()
    var element: NSString = ""
    
    //MARK: Parsing XML
    
    func parseQuizXML(category: String) {
        self.quizDict.removeAll()
        self.questionDict = NSMutableDictionary()
        let path = ((Bundle.main.resourcePath)! as NSString).appendingPathComponent(category.appending(".xml"))
        let data = try? Data(contentsOf: URL(fileURLWithPath: path))
        
        let xmlParser = XMLParser(data: data!)
        xmlParser.delegate = self
        
        xmlParser.shouldProcessNamespaces = false
        xmlParser.shouldReportNamespacePrefixes = false
        xmlParser.shouldResolveExternalEntities = false
        
        xmlParser.parse()
    }
    
    func parseQuizXML() {
        self.quizDict.removeAll()
        self.questionDict = NSMutableDictionary()
        let path = ((Bundle.main.resourcePath)! as NSString).appendingPathComponent(".xml")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path))
        
        let xmlParser = XMLParser(data: data!)
        xmlParser.delegate = self
        
        xmlParser.shouldProcessNamespaces = false
        xmlParser.shouldReportNamespacePrefixes = false
        xmlParser.shouldResolveExternalEntities = false
        
        xmlParser.parse()
    }
    
    //MARK: XMLParserDelegate
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        element = elementName as NSString
        if elementName == "question" {
            if (questionDict.count > 0) {
                questionDict.removeAllObjects()
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if (element == "option1" || element == "option2" ||
            element == "option3" || element == "answer" || element == "PlayerName" || element == "contentType" || element == "level" ||
            element == "image" || element == "text" || element == "id") {
            questionDict.setObject(string, forKey: element)
            element = ""
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "question" {
            let aQuestion = Question(dictionary: questionDict)
            let keys = Array(self.quizDict.keys)
            if keys.contains(aQuestion.level) {
                var array = self.quizDict[aQuestion.level]
                array?.append(aQuestion)
                self.quizDict[aQuestion.level] = array
            }
            else {
                var array = [Question]()
                array.append(aQuestion)
                self.quizDict[aQuestion.level] = array
            }
        } else if elementName == "quiz" {
            self.delegate?.questionsReturned(questions: self.quizDict)
        }
    }
}
