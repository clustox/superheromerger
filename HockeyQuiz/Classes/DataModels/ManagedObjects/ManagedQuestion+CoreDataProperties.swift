//
//  ManagedQuestion+CoreDataProperties.swift
//  HockeyQuiz
//
//  Created by Mac on 15/06/2017.
//  Copyright © 2017 Saira. All rights reserved.
//

import Foundation
import CoreData


extension ManagedQuestion {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedQuestion> {
        return NSFetchRequest<ManagedQuestion>(entityName: "Question")
    }

    @NSManaged public var id: Int64
    @NSManaged public var answer: String?
    @NSManaged public var level: Int64
    @NSManaged public var category: String?
    @NSManaged public var isVisited: Bool
    @NSManaged public var optionOne: String?
    @NSManaged public var optionTwo: String?
    @NSManaged public var optionThree: String?
    @NSManaged public var isCorrect: Bool
    @NSManaged public var isSkipped: Bool
    @NSManaged public var playerName: String?
}
