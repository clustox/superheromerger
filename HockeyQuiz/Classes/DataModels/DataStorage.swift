//
//  DataStorage.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import UIKit

class DataStorage: NSObject, XMLParserDelegate {
    // Parser
    let parser = DataParser()
    let user = User()
    static let shared = DataStorage()
    
    var quizDict = [Int: [Question]]()
    var moreApps = NSMutableDictionary()
    var maxNo: Int = -1
    var currentQs: Int = -1
    var version = 1.0
    var level = 0
    var getRandom = false
    var soundOff = false
    var category = Category()
    var quizCount = 0
    var categories = [Category]()
    var totalLevels = 0
    
    fileprivate override init() {
        super.init()
    }
    
    func loadFromDisk() {
        
        _ = DatabaseHandler.clearData()
        
        let category = DataStorage.shared.category.categoryName.replacingOccurrences(of: " ", with: "_")
        let levelKey = category.appending("_level")
        
        let theLastLevel = UserDefaults.standard.integer(forKey: levelKey)
        if theLastLevel > 0 {
            self.user.currentLevel = theLastLevel
        }
        
        self.soundOff = UserDefaults.standard.bool(forKey: "soundOff")
        
        self.loadCategories()
        self.updateMoreApps()
        
    }
    
    func loadCategories() {
        
        if let path = Bundle.main.path(forResource: "Categories", ofType: "plist") {
            
            if let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
                
                let keys = Array(dict.keys.map{String($0)}).sorted(by: {$0 < $1})
                
                for key in keys {
                    let array = dict[key] as! [String]
                    let category = Category.init()
                    category.categoryName = (array[0])
                    category.imageSource = array[1]
                    category.quizSource = array[2]
                    
                    self.categories.append(category)
                }
            }
            
        }
        
    }
    
    func loadQuiz(){
        let quizSource = category.quizSource.replacingOccurrences(of: " ", with: "-")
        parser.delegate = self
        parser.parseQuizXML(category: quizSource)
    }
    
    func saveToDisk(score: Int) {
        
        let category = DataStorage.shared.category.categoryName.replacingOccurrences(of: " ", with: "_")
        let bestScoreKey = category.appending("_best_score")
        
        UserDefaults.standard.set(self.soundOff, forKey: "soundOff")
        
        let theHighScore = UserDefaults.standard.integer(forKey: bestScoreKey)
        
        if score > theHighScore {
            UserDefaults.standard.set(NSNumber(value: score as Int), forKey: bestScoreKey)
        }
        
        self.setLevel()
    }
    
    
    func setLevel() {
        let category = DataStorage.shared.category.categoryName.replacingOccurrences(of: " ", with: "_")
        let key = category.appending("_level")
        UserDefaults.standard.set(NSNumber(value: self.level as Int), forKey: key)
    }
    
    //MARK: Load More Apps
    func updateMoreApps() {
        autoreleasepool {
            let thePath = Bundle.main.path(forResource: "MoreApps", ofType: "plist")
            if thePath != nil {
                let tempDict = NSMutableDictionary(contentsOfFile: thePath!)
                if tempDict != nil {
                    if (UserDefaults.standard.object(forKey: "version") != nil) {
                        self.version = ((UserDefaults.standard.object(forKey: "version") as AnyObject).doubleValue)!
                    }
                    self.moreApps.addEntries(from: tempDict?.object(forKey: "MoreApps") as! [AnyHashable: Any])
                }
            }
        }
    }
    
    //MARK: Reset Values
    
    func resetValues(forLevel: Bool) {
        maxNo = 0;
        self.level = 0;
        self.currentQs = -1;
        self.getRandom = false
        
        guard let navigationController = UIApplication.appDelegate?.window?.rootViewController as? UINavigationController,
            let homeViewController = navigationController.viewControllers[0] as? HomeScreenViewController else {
                return
        }
        homeViewController.enablePlayButtonsWhenQuizIsReady()
        if DatabaseHandler.clearData() {
            self.fillQuizData()
        }
    }
    
    func fillQuizData() {
        self.quizDict.removeAll()
        if !DatabaseHandler.loadQuiz(category: self.category) {
            self.loadQuiz()
        } else{
            if DatabaseHandler.clearData() {
                self.loadQuiz()
            }
        }
    }
    
    //MARK: GameEnd
    func checkForGameEnd(forLevel: Bool) -> Bool {
        if forLevel {
            guard let array = self.quizDict[self.level] else {
                return true
            }
//            if self.quizCount == array!.count - 1 {
//                return true
//            }
//            return false
            if array.count > 0 {
                return false
            }
            return true
        } else {
            if self.quizDict.count == 0 {
                guard let navigationController = UIApplication.appDelegate?.window?.rootViewController as? UINavigationController,
                    let homeViewController = navigationController.viewControllers[0] as? HomeScreenViewController else {
                        return true
                }
                homeViewController.enablePlayButtonsWhenQuizIsReady()
                return true
            }
            return false
        }
    }
    
    //MARK: Next Question
    func calculateNextQs(forLevel: Bool) {
        self.quizCount += 1
        if (forLevel){
            self.randomQuestion()
        }else {
            self.randomLevelRandomQs()
        }
    }
    
    func randomLevelRandomQs() {
        let allKeys = Array(self.quizDict.keys)
        let sortedArray = allKeys.sorted()
        if sortedArray.count > 1 {
            let number = self.randomNumberBetweenTwoNumbers(0, largest: UInt32(sortedArray.count-1))
            self.level = allKeys[number]
        }
        else {
            self.level = sortedArray.last!
        }
        
        self.randomQuestion()
    }
    
    func randomQuestion() {
        let array = self.quizDict[self.level]
        if array?.count == 1 {
            self.currentQs = 0
        }
        else {
            self.currentQs = self.randomNumberBetweenTwoNumbers(0, largest: UInt32(array!.count-1))
        }
    }
    
    func randomNumberBetweenTwoNumbers(_ smallest: UInt32, largest: UInt32) -> Int {
        let randomNumber = arc4random_uniform(largest - smallest) + smallest
        return Int(randomNumber);
    }
    
    func deleteAnsweredQuestion(forLevel: Bool) {
        if self.currentQs >= 0 {
            //To avoid repeating questions, remove answered question from main quiz dict
            //and add to another structure answeredQsDict,
            //at end of game move questions from answeredQsDict back to quizDict.
            var array = self.quizDict[self.level]
            if (array != nil && self.currentQs <= array!.count-1) {
                array!.remove(at: self.currentQs)
                self.quizDict[self.level] = array
                if array!.count == 0 {
                    self.quizDict.removeValue(forKey: self.level)
                }
            }
        }
        
    }
}

extension DataStorage: DataParserDelegate {
    /// Callback function for parsing questions from given xml
    ///
    /// - Parameters:
    ///   - events: List of questions arranged on levels if succesfully created else empty List
    internal func questionsReturned(questions: [Int : [Question]]) {
        self.quizDict = questions
        self.totalLevels = self.quizDict.keys.count
        if DatabaseHandler.insertQuestions(questions: questions,category: self.category) {
            self.parser.delegate = nil
            guard let navigationController = UIApplication.appDelegate?.window?.rootViewController as? UINavigationController,
                let homeViewController = navigationController.viewControllers[0] as? HomeScreenViewController else {
                    return
            }
            homeViewController.enablePlayButtonsWhenQuizIsReady()
        }
    }
}
